angular
  .module('nhapp')
  .service('notificationService', notificationService);

notificationService.$inject = ['$ionicPopup', '$state']

function notificationService($ionicPopup, $state) {
	var vm = this;
	vm.alert = alert;

	function alert(title,alertMsg,okAction){
	    $ionicPopup.alert({
     		title: title,
     		template: alertMsg
   		}).then(okAction);
  	}
}