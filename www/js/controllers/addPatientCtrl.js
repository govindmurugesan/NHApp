(function () {
  'use strict';

  angular
    .module('nhapp')
    .controller('addPatientCtrl', addPatientCtrl);

  addPatientCtrl.$inject = ['$scope', '$state', '$window', '$ionicHistory', '$ionicPopup', 'notificationService'];

  function addPatientCtrl($scope, $state, $window, $ionicHistory, $ionicPopup, notificationService){
  	var vm = this;
  	vm.submit = submit;
    vm.showPopup = showPopup;
  	var _patientDetails = [];

  	$scope.$on('$ionicView.enter', function () {
	  	if (!$window.localStorage.patientDetails){
	        _patientDetails = [];
	    }else{
	    	_patientDetails = [];
	        angular.forEach(JSON.parse($window.localStorage.patientDetails), function (piece, index) {
            _patientDetails.push({
              Name:piece.Name,
	  			    Age:piece.Age,
	  			    Gender:piece.Gender,
	  			    Address:piece.Address,
	  			    Camp:piece.Camp,
	  			    PatientProblem:piece.PatientProblem
            })
	        });
	    }
    })

    function showPopup(){
      var popup = $ionicPopup.show({
        'templateUrl': 'templates/filterOptions.html',
        title: 'Camp',
        scope: $scope,
        'buttons': [
          {
            'text': 'Cancel',
            'type': 'button-positive',
            'onTap': function (event) {
              event.preventDefault();
              popup.close();
            }
          },
          {
            'text': 'Ok',
            'type': 'button-positive',
            'onTap': function (event) {
              vm.camp = vm.filterOption;
              popup.close();
              event.preventDefault()
     
            }
          }
        ]
      });
    }

  	function submit(){
      if((vm.name == '' || vm.name == undefined) || (vm.age == ''|| vm.age == undefined) || (vm.gender == ''|| vm.gender == undefined) || (vm.address == ''|| vm.address == undefined) || (vm.camp == '' || vm.camp == undefined) || (vm.problem == '' || vm.problem == undefined)){
        notificationService.alert('',"All fields are mandatory", null);
      }else{
        _patientDetails.push({
          Name:vm.name,
          Age:vm.age,
          Gender:vm.gender,
          Address:vm.address,
          Camp:vm.camp,
          PatientProblem:vm.problem
        })
        $window.localStorage.patientDetails = JSON.stringify(_patientDetails);
        vm.name = '';
        vm.age = '';
        vm.gender = '';
        vm.address = '';
        vm.camp = '';
        vm.problem = '';
        notificationService.alert('',"Details added successfully",function(){
          $ionicHistory.nextViewOptions({
            disableBack: true
          });
          $state.go('app.dashboard');
        })
      }
    }
  }

})();