(function () {
  'use strict';

  angular
    .module('nhapp')
    .controller('dashboardCtrl', dashboardCtrl);

  dashboardCtrl.$inject = ['$scope', '$state', '$window'];

  function dashboardCtrl($scope, $state, $window){
  	$scope.labels = ["Sarjapur", "E.City", "Silkboard", "ITPL", "KR Puram", "Majestic"];
  	$scope.series = ['No. of Patients'];
  	$scope.data = [
  		[28, 48, 19, 76, 27, 50]
  	];
  	$scope.onClick = function (points, evt) {
  		console.log(points, evt);
  	};
  }

})();