(function () {
  'use strict';

  angular
    .module('nhapp')
    .controller('campDetailsCtrl', campDetailsCtrl);

  campDetailsCtrl.$inject = ['$scope', '$state', '$window', '$ionicPopup'];

  function campDetailsCtrl($scope, $state, $window, $ionicPopup){
  	var vm = this;
  	vm.showPopup = showPopup;
  	vm.patientDetails = [];
  	vm.filterOption = 'All';
  	vm.excelReport = excelReport;
  	var logOb;

  	$scope.$on('$ionicView.beforeEnter', function () {
	  	if (!$window.localStorage.patientDetails){
	        vm.patientDetails = [];
	    }else{
	    	vm.patientDetails = [];
	        angular.forEach(JSON.parse($window.localStorage.patientDetails), function (piece, index) {
	            vm.patientDetails.push({
	                Name:piece.Name,
		  			Age:piece.Age,
		  			Gender:piece.Gender,
		  			Address:piece.Address,
		  			Camp:piece.Camp,
		  			PatientProblem:piece.PatientProblem
	              })
	        });
	    }
	})

  	function showPopup(){
  		var popup = $ionicPopup.show({
	        'templateUrl': 'templates/filterOptions.html',
	        title: 'Camps',
	        scope: $scope,
	        'buttons': [
	          {
	            'text': 'Cancel',
	            'type': 'button-positive',
	            'onTap': function (event) {
	              event.preventDefault();
	              popup.close();
	            }
	          },
	          {
	            'text': 'Ok',
	            'type': 'button-positive',
	            'onTap': function (event) {
	            	if (!$window.localStorage.patientDetails){
				        vm.patientDetails = [];
				    }else{
				    	vm.patientDetails = [];
		            	angular.forEach(JSON.parse($window.localStorage.patientDetails), function (piece, index) {
		            		if(piece.Camp == vm.filterOption){
		            			vm.patientDetails.push({
					                Name:piece.Name,
						  			Age:piece.Age,
						  			Gender:piece.Gender,
						  			Address:piece.Address,
						  			Camp:piece.Camp,
						  			PatientProblem:piece.PatientProblem
				              	})
		            		}
				        });
	            	}
	              popup.close();
	              event.preventDefault()
	     
	            }
	          }
	        ]
      	});

  	}

  	function excelReport(){

  		window.resolveLocalFileSystemURL(cordova.file.externalRootDirectory, function(dir) {
	        console.log("got main dir",dir);
	        dir.getFile("log.txt", {create:true}, function(file) {
	            console.log("got the file", file);
	            logOb = file;
	            writeLog("App started");          
	        });
	    });

  		/*var type = window.PERSISTENT;
	   var size = 5*1024*1024;

	   window.requestFileSystem(type, size, successCallback, errorCallback)

	   function successCallback(fs) {
	      fs.root.getFile('success.txt', {create: true, exclusive: true}, function(fileEntry) {
	         alert('File creation successfull!')
	      }, errorCallback);
	   }

	   function errorCallback(error) {
	      alert("ERROR: " + error.code)
	   }*/
  		//var req = alasql('SELECT * INTO XLSX("report.xls",{headers:true}) FROM ?',[vm.patientDetails]);
  		console.log('window.plugins',cordova.file.externalRootDirectory);
  		console.log("File downloaded to "+cordova.file.dataDirectory);

  		/*var attachData= ['file:///data/data/com.ionicframework.nhapp965271/files/log.txt'];
  		if(window.plugins && window.plugins.emailComposer) {
            window.plugins.emailComposer.showEmailComposerWithCallback(function(result) {
                console.log("Response -> " + result);
            }, 
            "Feedback for your App", // Subject
            "",                      // Body
            ["yrahulkumar@saptalabs.com"],    // To
            null,                    // CC
            null,                    // BCC
            false,                   // isHTML
            attachData,                    // Attachments
            null);                   // Attachment Data
        }

  		console.log('excel');*/

  		/*var blob = new Blob([vm.patientDetails], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
        saveAs(blob, "Report.xls");*/

        /*cordova.plugins.email.open({
        	app: 'gmail',
		    to:      'max@mustermann.de',
		    cc:      'erika@mustermann.de',
		    bcc:     '',
		    subject: 'Greetings',
		    body:    'How are you? Nice greetings from Leipzig',
		    attachments: 'file:///storage/sdcard1/00002.vcf', //file:///storage/emulated/0/=> Android 
		});*/

  	}

  	function writeLog(str) {
	    if(!logOb) return;
	    var log = str + " [" + (new Date()) + "]\n";
	    console.log("going to log "+log);
	    logOb.createWriter(function(fileWriter) {
	        
	        fileWriter.seek(fileWriter.length);
	        
	        var blob = new Blob([log], {type:'text/plain'});
	        fileWriter.write(blob);
	        console.log("ok, in theory i worked");
	        var attachData= [cordova.file.externalRootDirectory+'log.txt'];
	        console.log('attachData',attachData);
	  		/*if(window.plugins && window.plugins.EmailComposer) {*/console.log('inside');
	            window.plugins.EmailComposer.showEmailComposerWithCallback(function(result) {
	                console.log("Response -> " + result);
	            }, 
	            "Feedback for your App", // Subject
	            "",                      // Body
	            ["yrahulkumar@saptalabs.com"],    // To
	            null,                    // CC
	            null,                    // BCC
	            false,                   // isHTML
	            attachData);                   // Attachment Data
	        /*}*/
	    }, fail);
	}
	function fail(e) {
		console.log("FileSystem Error");
		console.dir(e);
	}

	function jsonToSsXml(jsonObject) {
		    var row;
		    var col;
		    var xml;
		    var data = typeof jsonObject != "object" ? JSON.parse(jsonObject) : jsonObject;
		    
		    xml = emitXmlHeader();

		    for (row = 0; row < data.length; row++) {
		        xml += '<ss:Row>\n';
		      
		        for (col in data[row]) {
		            xml += '  <ss:Cell>\n';
		            xml += '    <ss:Data ss:Type="' + testTypes[col]  + '">';
		            xml += data[row][col] + '</ss:Data>\n';
		            xml += '  </ss:Cell>\n';
		        }

		        xml += '</ss:Row>\n';
		    }
		    
		    xml += emitXmlFooter();
		    return xml;  
		};

		function emitXmlHeader() {
		    var headerRow =  '<ss:Row>\n';
		    for (var colName in testTypes) {
		        headerRow += '  <ss:Cell>\n';
		        headerRow += '    <ss:Data ss:Type="String">';
		        headerRow += colName + '</ss:Data>\n';
		        headerRow += '  </ss:Cell>\n';        
		    }
		    headerRow += '</ss:Row>\n';    
		    return '<?xml version="1.0" encoding="Windows-1252"?>\n' +
		           '<ss:Workbook xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet">\n' +
		           '<ss:Worksheet ss:Name="Sheet1">\n' +
		           '<ss:Table>\n\n' + headerRow;
		};

		function emitXmlFooter() {
		    return '\n</ss:Table>\n' +
		           '</ss:Worksheet>\n' +
		           '</ss:Workbook>\n';
		};

  }

})();